## PROJECT SETUP
cmake_minimum_required(VERSION 3.9 FATAL_ERROR)
project(Rat-DistmeshCPP VERSION 1.000.0 LANGUAGES CXX)

# switchboard
option(ENABLE_TESTING "Build unit/system tests" ON)
option(ENABLE_EXAMPLES "Build examples" ON)

# set build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# compiler options
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wpedantic -Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3")
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# ensure out of core build
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.\n")
endif()

## EXTERNAL LIBRARIES
# find the rat common library
find_package(RatCommon 1.0 REQUIRED)

## LIBRARY
# add the library
add_library(ratdmsh SHARED
	src/dfairfoil.cpp
	src/dfcircle.cpp
	src/dfdiff.cpp
	src/dfellipse.cpp
	src/dfintersect.cpp
	src/dfones.cpp
	src/dfpolygon.cpp
	src/dfrectangle.cpp
	src/dfrrectangle.cpp
	src/dfscale.cpp
	src/dfunion.cpp
	src/distmesh2d.cpp
	src/serializer.cpp
)

# Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Rat::Dmsh ALIAS ratdmsh)

# properties
set_target_properties(ratdmsh PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(ratdmsh PROPERTIES SOVERSION 1)

# add include directory
target_include_directories(ratdmsh
	PUBLIC 
		$<INSTALL_INTERFACE:include>    
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

# link libraries
target_link_libraries(ratdmsh PUBLIC Rat::Common)

## UNIT/SYSTEM TESTS
# check switchboard
if(ENABLE_TESTING)
	# use cmake's platform for unit testing
	enable_testing()

	# add test directory
	add_subdirectory(test)
endif()

## EXAMPLES
# check switchboard
if(ENABLE_EXAMPLES)
	# add example directory
	add_subdirectory(examples)
endif()

# copy database files to build directory
file(COPY json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

## INSTALLATION
# get install directories
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ratdmsh)

install(TARGETS ratdmsh
	EXPORT ratdmsh-targets
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
)

# This is required so that the exported target has the name RatCmn and not ratcmn
set_target_properties(ratdmsh PROPERTIES EXPORT_NAME Dmsh)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/dmsh)

# Export the targets to a script
install(EXPORT ratdmsh-targets
	FILE
		RatDmshTargets.cmake
	NAMESPACE
		Rat::
	DESTINATION
		${INSTALL_CONFIGDIR}
)

# Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/RatDmshConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/RatDmshConfig.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/RatDmshConfig.cmake
	INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

# Install the config, configversion and custom find modules
install(FILES
	${CMAKE_CURRENT_BINARY_DIR}/RatDmshConfig.cmake
	${CMAKE_CURRENT_BINARY_DIR}/RatDmshConfigVersion.cmake
	DESTINATION ${INSTALL_CONFIGDIR}
)

## EXPORT
export(EXPORT ratdmsh-targets
	FILE ${CMAKE_CURRENT_BINARY_DIR}/RatDmshTargets.cmake
	NAMESPACE Rat::)

# Register package in user's package registry
# export(PACKAGE RatDmsh)

#####################
# Setup install paths
#####################

include(GNUInstallDirs)

# Set RPATHS
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
list(APPEND CMAKE_INSTALL_RPATH "$ORIGIN/../${CMAKE_INSTALL_LIBDIR}")

# Create Debian package, must be the last thing
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
include(GenerateDebianPackage)
