/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfscale.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFScale::DFScale(ShDistFunPr df, const double offset, const double scale, const double max){
		if(df==NULL)rat_throw_line("distance function points to NULL");
		df_ = df; offset_ = offset; scale_ = scale; max_ = max;
	}

	// factory
	ShDFScalePr DFScale::create(ShDistFunPr df, const double offset, const double scale, const double max){
		if(df==NULL)rat_throw_line("distance function points to NULL");
		return std::make_shared<DFScale>(df,offset,scale,max);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFScale::get_bounding() const{
		if(df_==NULL)rat_throw_line("distance function not set");
		// return box
		return df_->get_bounding();
	}

	// get fixed points
	arma::Mat<double> DFScale::get_fixed() const{
		if(df_==NULL)rat_throw_line("distance function not set");
		return df_->get_fixed();
	}


	// distance function
	arma::Col<double> DFScale::calc_distance(const arma::Mat<double> &p) const{
		return arma::min(offset_ + scale_*df_->calc_distance(p),arma::Col<double>(p.n_rows,arma::fill::ones)*max_);
	}

	// type string for serialization
	std::string DFScale::get_type(){
		return "rat::dm::dfscale";
	}

	// method for serialization into json
	void DFScale::serialize(Json::Value &js, cmn::SList &list) const{
		// store type
		js["type"] = get_type();

		// scaling
		js["offset"] = offset_;
		js["scale"] = scale_;
		js["max"] = max_;

		// serialize distance functions
		js["distfun"] = cmn::Node::serialize_node(df_,list);
	}

	// method for deserialisation from json
	void DFScale::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// variables
		offset_ = js["offset"].asDouble();
		scale_ = js["scale"].asDouble();
		max_ = js["max"].asDouble();
		
		// distance function
		df_ = Node::deserialize_node<DistFun>(js["distfun"], list, factory_list, pth);
	}

}}