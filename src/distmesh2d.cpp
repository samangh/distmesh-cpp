/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "distmesh2d.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DistMesh2D::DistMesh2D(){
		set_h0(0.2);

		// default scaling fun (no scaling)
		fh_ = DFOnes::create();
	}

	// factory
	ShDistMesh2DPr DistMesh2D::create(){
		return std::make_shared<DistMesh2D>();
	}

	// meshgrid creates an x,y grid from input arrays
	// same as meshgrid in matlab
	void DistMesh2D::meshgrid(arma::Mat<double> &x, arma::Mat<double> &y, 
		const arma::Row<double> &xgv, const arma::Col<double> &ygv){

		// allocate coordinates
		x.set_size(ygv.n_elem,xgv.n_elem);
		y.set_size(ygv.n_elem,xgv.n_elem);

		// set coordinates to matrix
		x.each_row() = xgv;
		y.each_col() = ygv;
	}

	// circumcircle creation
	// translated from from Paul Bourke 
	// http://paulbourke.net/papers/triangulate/
	bool DistMesh2D::circumcircle(
		arma::Row<double>::fixed<2> &pc, double &r,
		const arma::Row<double>::fixed<2> &p, 
		const arma::Mat<double>::fixed<3,2> &ptri){

		//  get points
		const double x1 = ptri(0,0);
		const double y1 = ptri(0,1);
		const double x2 = ptri(1,0);
		const double y2 = ptri(1,1);
		const double x3 = ptri(2,0);
		const double y3 = ptri(2,1);

		// allocate circle centers
		double xc,yc;

		// check for coincident points
		if(std::abs(y1 - y2) < arma::datum::eps && std::abs(y2 - y3) < arma::datum::eps)
			return(false);

		// not coincident
		if(std::abs(y2-y1) < arma::datum::eps){ 
			const double m2 = - (x3 - x2) / (y3 - y2);
			const double mx2 = (x2 + x3) / 2.0;
			const double my2 = (y2 + y3) / 2.0;
			xc = (x2 + x1) / 2.0;
			yc = m2 * (xc - mx2) + my2;
		}else if(std::abs(y3 - y2) < arma::datum::eps){ 
			const double m1 = - (x2 - x1) / (y2 - y1);
			const double mx1 = (x1 + x2) / 2.0;
			const double my1 = (y1 + y2) / 2.0;
			xc = (x3 + x2) / 2.0;
			yc = m1 * (xc - mx1) + my1;
		}else{
			const double m1 = - (x2 - x1) / (y2 - y1); 
			const double m2 = - (x3 - x2) / (y3 - y2); 
			const double mx1 = (x1 + x2) / 2.0; 
			const double mx2 = (x2 + x3) / 2.0;
			const double my1 = (y1 + y2) / 2.0;
			const double my2 = (y2 + y3) / 2.0;
			xc = (m1 * mx1 - m2 * mx2 + my2 - my1) / (m1 - m2); 
			yc = m1 * (xc - mx1) + my1; 
		}
		
		// set circle center point
		pc(0) = xc;	pc(1) = yc;

		// check 
		double dx = x2 - pc(0);
		double dy = y2 - pc(1);
		double rsqr = dx*dx+dy*dy;
		r = std::sqrt(rsqr); 
		dx = p(0) - xc;
		dy = p(1) - yc;
		double drsqr = dx*dx + dy*dy;
		// return((drsqr <= rsqr) ? true : false);
		return((drsqr - rsqr) <= arma::datum::eps ? true : false);
	}


	// delaunay triangulation
	// translated from from Paul Bourke 
	// http://paulbourke.net/papers/triangulate/
	arma::Mat<arma::uword> DistMesh2D::triangulation(const arma::Mat<double> &p){
		// settings
		arma::uword emax = 200; // edge buffer

		// get number of points
		arma::uword num_points = p.n_rows;
		arma::uword num_tri_max = 4*num_points;

		// allocate output
		arma::Mat<arma::uword> tri(num_tri_max,3);
		arma::uword num_tri = 0;

		// allocate edges
		arma::Mat<arma::sword> edges(emax,2);
		arma::uword num_edges = 0;

		// Find the maximum and minimum vertex bounds.
		// This is to allow calculation of the bounding triangle
		arma::Row<double> pmax = arma::max(p,0);
		arma::Row<double> pmin = arma::min(p,0);

		// size of box
		arma::Row<double> dp = pmax-pmin;

		// find maximum dimension
		double dmax = arma::max(dp);

		// midpoint
		arma::Row<double> pmid = (pmax + pmin)/2;

		// Set up the supertriangle
		// his is a triangle which encompasses all the sample points.
		// The supertriangle coordinates are added to the end of the
		// vertex list. The supertriangle is the first triangle in
		// the triangle list.
		arma::Mat<double>::fixed<3,2> psuper;
		psuper(0,0) = pmid(0) - 20*dmax;
		psuper(0,1) = pmid(1) - dmax;
		psuper(1,0) = pmid(0);
		psuper(1,1) = pmid(1) + 20*dmax;
		psuper(2,0) = pmid(0) + 20*dmax;
		psuper(2,1) = pmid(1) - dmax;

		// sort indexes for p
		arma::Col<arma::uword> sort_index = arma::sort_index(p.col(0));

		// combine points
		arma::Mat<double> pext = arma::join_vert(p.rows(sort_index),psuper);

		// set first triangle
		tri(0,0) = num_points;
		tri(0,1) = num_points+1;
		tri(0,2) = num_points+2;
		num_tri = 1;

		// keep track of completed triangles
		arma::Col<arma::uword> is_complete(num_tri_max);
		is_complete(0) = false;

		// include each point one at a time into the existing mesh
		for(arma::uword i=0;i<num_points;i++){
			// get point from list
			arma::Row<double>::fixed<2> pnt = pext.row(i);
			num_edges = 0;

			// Set up the edge buffer.
			// If the point (xp,yp) lies inside the circumcircle then the
			// three edges of that triangle are added to the edge buffer
			// and that triangle is removed.
			// walk over triangles
			for(arma::uword j=0;j<num_tri;j++){
				// check if is already complete
				if(is_complete(j))continue;

				// get points for this triangle from list
				arma::Mat<double>::fixed<3,2> ptri = pext.rows(tri.row(j));

				// check if point is inside circle
				arma::Row<double>::fixed<2> pc = {0,0}; double rc = 0;
				bool inside = circumcircle(pc,rc,pnt,ptri);

				// check if complete
				if (pc(0) + rc < pnt(0)){
					is_complete(j)=true;
				}

				// if point is inside circle
				if(inside){
					// check if edge list full
					if(num_edges+3>=emax){emax += 100; edges.resize(emax,2);}
					
					// add new edges to list
					edges(num_edges+0,0) = tri(j,0);
					edges(num_edges+0,1) = tri(j,1);
					edges(num_edges+1,0) = tri(j,1);
					edges(num_edges+1,1) = tri(j,2);
					edges(num_edges+2,0) = tri(j,2);
					edges(num_edges+2,1) = tri(j,0);
					num_edges+=3;

					// copy last triangle and reduce list
					tri.row(j) = tri.row(num_tri-1);
					is_complete(j) = is_complete(num_tri-1);
					num_tri--; j--;
				}
			}


			// Tag multiple edges
			// Note: if all triangles are specified anticlockwise then all
			// interior edges are opposite pointing in direction.
			for(arma::uword j=0;j<num_edges-1;j++){
				for(arma::uword k = j+1;k<num_edges;k++){
					if((edges(j,0) == edges(k,1)) && (edges(j,1) == edges(k,0))){
						edges.row(j).fill(-1); edges.row(k).fill(-1);
					}

					// Shouldn't need the following, see note above
					else if((edges(j,0) == edges(k,0)) && (edges(j,1) == edges(k,1))){
						edges.row(j).fill(-1); edges.row(k).fill(-1);
					}
				}
			}

			// Form new triangles for the current point
			// Skipping over any tagged edges.
			// All edges are arranged in clockwise order.
			for(arma::uword j=0;j<num_edges;j++) {
				 // check if tagged
				 if(arma::any(edges.row(j)<0))continue;

				 // add edges to new triangle
				 tri(num_tri,0) = edges(j,0);
				 tri(num_tri,1) = edges(j,1);
				 tri(num_tri,2) = i;
				 is_complete(num_tri) = false;
				 num_tri++;
			}
		}


		// Remove triangles with supertriangle vertices
		// These are triangles which have a vertex number greater than nv
		for(arma::uword i=0;i<num_tri;i++){
			if(arma::any(tri.row(i)>=num_points)){
				tri.row(i) = tri.row(num_tri-1);
				num_tri--; i--;
			}
		}

		// change size
		tri.resize(num_tri,3);

		// get unique rows
		// tri = cmn::Extra::unique_rows(tri);

		// unsort tri
		arma::Col<arma::uword> original_index = arma::regspace<arma::Col<arma::uword> >(0,num_points-1);
		original_index = original_index.rows(sort_index);
		tri = arma::reshape(original_index.rows(arma::vectorise(tri)),num_tri,3);

		// return triangulation
		return tri;
	}

	// element graph
	void DistMesh2D::element_graph(
		arma::Col<arma::uword> &row, 
		arma::Col<arma::uword> &col, 
		const arma::Mat<arma::uword> &t){

		// number of elements
		const arma::uword num_tri = t.n_rows;

		// Interior bars duplicated
		arma::Mat<arma::uword> e(3*num_tri,2);
		arma::Col<arma::uword> id(3*num_tri);

		// create edges
		e.rows(0*num_tri,1*num_tri-1) = t.cols(arma::Row<arma::uword>{0,1});
		e.rows(1*num_tri,2*num_tri-1) = t.cols(arma::Row<arma::uword>{0,2});
		e.rows(2*num_tri,3*num_tri-1) = t.cols(arma::Row<arma::uword>{1,2});

		// keep track of element indices
		id.rows(0*num_tri,1*num_tri-1) = arma::regspace<arma::Col<arma::uword> >(0,num_tri-1);
		id.rows(1*num_tri,2*num_tri-1) = arma::regspace<arma::Col<arma::uword> >(0,num_tri-1);
		id.rows(2*num_tri,3*num_tri-1) = arma::regspace<arma::Col<arma::uword> >(0,num_tri-1);

		// sort edges
		arma::Mat<arma::uword> es = arma::sort(e,"ascend",1);

		// sort rows
		for(arma::uword i=0;i<es.n_cols;i++){
			const arma::Col<arma::uword> sort_index = arma::stable_sort_index(es.col(i));
			es = es.rows(sort_index); id = id.rows(sort_index);
		} 

		// allocate graph
		row.set_size(3*num_tri); col.set_size(3*num_tri);

		// walk over edges
		arma::uword cnt = 0;
		for(arma::uword i=1;i<es.n_rows;i++){
			// same edge
			if(arma::all(es.row(i)==es.row(i-1))){
				//row(cnt) = id(i); col(cnt) = id(i-1); cnt++;
				row(cnt) = id(i-1); col(cnt) = id(i); cnt++;
			}
		}

		// resize
		row.resize(cnt); 
		col.resize(cnt);
	}

	// calculate element quality
	arma::Col<double> DistMesh2D::element2quality(
		const arma::Mat<arma::uword> &tq, const arma::Mat<double> &p){
		// number of elements
		const arma::uword num_elements = tq.n_rows;
		const arma::uword num_sides = tq.n_cols;

		// angle of ideal polygon
		double poly_angle = (num_sides-2)*arma::datum::pi;
		double corner_angle = poly_angle/num_sides;

		// allocate
		arma::Col<double> Q(num_elements);

		// walk over elements
		for(arma::uword i=0;i<num_elements;i++){
			// allocate corners
			arma::Row<double> theta(num_sides);

			// calculate corners
			for(arma::uword j=0;j<num_sides;j++){
				// get edge vectors
				const arma::Row<double>::fixed<2> v1 = p.row(tq(i,(j+1)%num_sides)) - p.row(tq(i,(j+0)%num_sides));
				const arma::Row<double>::fixed<2> v2 = p.row(tq(i,(j+2)%num_sides)) - p.row(tq(i,(j+1)%num_sides));
				
				// dot product
				double costheta = -arma::sum(v1%v2)/(std::sqrt(arma::sum(v1%v1))*std::sqrt(arma::sum(v2%v2)));
				
				// fix bounds
				if(costheta<-1)costheta = -1;
				if(costheta>1)costheta = 1;

				// calcualte angle
				theta(j) = std::acos(costheta);
			}

			// concave elements
			if(std::abs(arma::sum(theta)-poly_angle)>=1e-4){
				Q(i) = -1.0;
			}else{
				Q(i) = arma::prod(1.0 - arma::abs((corner_angle - theta)/corner_angle));
			}
		}

		// return quality
		return Q;
	}


	// calculate element quality
	arma::Col<double> DistMesh2D::element2quality_alt(
		const arma::Mat<arma::uword> &tq, const arma::Mat<double> &p){
		// get two common points
		const arma::Mat<double> p1 = p.rows(tq.col(1));
		const arma::Mat<double> p3 = p.rows(tq.col(3));

		// calculate cross vector
		arma::Mat<double> v = p3-p1;

		// normalize
		v.each_col()/=arma::sqrt(arma::sum(v%v,1));

		// check direction
		const arma::Row<double>::fixed<2> t1 = {0,1};
		const arma::Row<double>::fixed<2> t2 = {1,0};

		// dot product
		const arma::Col<double> vt1 = arma::sum(v.each_row()%t1,1);
		const arma::Col<double> vt2 = arma::sum(v.each_row()%t2,1);

		// allocate
		const arma::Col<double> Q = 1.0 - arma::max(arma::abs(vt1),arma::abs(vt2));

		// return quality
		return Q;
	}

	// merge triangles
	arma::Row<arma::uword>::fixed<4> DistMesh2D::merge2triangles(
		const arma::Row<arma::uword>::fixed<3> &t1, 
		const arma::Row<arma::uword>::fixed<3> &t2){
		
		// find common nodes
		arma::Mat<arma::uword>::fixed<3,3> M;
		for(arma::uword j=0;j<3;j++)
			M.row(j) = t1(j)==t2;
			
		// find common elements
		const arma::Col<arma::uword> cn1 = arma::sum(M,1);
		const arma::Row<arma::uword> cn2 = arma::sum(M,0);

		// ensure that the triangles can be merged
		assert(arma::sum(cn1)==2); assert(arma::sum(cn2)==2);

		// create quad
		arma::Row<arma::uword>::fixed<4> q;
		q(0) = arma::as_scalar(t1.cols(arma::find(cn1==false))); 
		q(1) = arma::as_scalar(t1.cols(arma::find(cn1==true,1,"first")));
		q(2) = arma::as_scalar(t2.cols(arma::find(cn2==false))); 
		q(3) = arma::as_scalar(t1.cols(arma::find(cn1==true,1,"last")));

		// return quad 
		return q;
	}

	// refinement algorithm (Catmull Clark subdivision)
	// output is pure quad mesh while input can be tri or quad
	void DistMesh2D::refine(arma::Mat<arma::uword> &qr, arma::Mat<double> &pr, 
		const arma::Mat<arma::uword> &qt, const arma::Mat<double> &p){

		// typoe of mesh
		arma::uword num_points = p.n_rows;
		arma::uword num_elements = qt.n_rows;
		arma::uword num_sides = qt.n_cols;

		// list of new points
		arma::Mat<double> pnew(num_elements*(num_sides+1),2,arma::fill::zeros);

		// list of new elements
		qr.set_size(num_elements*num_sides,4);

		// split nodes
		for(arma::uword i=0;i<num_elements;i++){
			// walk over vertices
			for(arma::uword j=0;j<num_sides;j++){
				// get indices
				arma::uword idx1 = qt(i,j); 
				arma::uword idx2 = qt(i,(j+1)%num_sides);

				// add new point on middle of each edge
				pnew.row(i*(num_sides+1)+j) = (p.row(idx1) + p.row(idx2))/2;
			}

			// new point on middle of element
			pnew.row((i+1)*(num_sides+1)-1) = arma::mean(p.rows(qt.row(i)),0);

			// connect new elements
			for(arma::uword j=0;j<num_sides;j++){
				// calculate shift due to element
				const arma::uword shft = num_points + i*(num_sides+1);

				// calculate last index
				arma::uword jmo; if(j==0)jmo=num_sides-1; else jmo = j-1;

				// add element
				qr.row(i*num_sides+j) = arma::Row<arma::uword>::fixed<4>{qt(i,j),shft+j,shft+num_sides,shft+jmo};
			}
		}

		// add points
		pr = arma::join_vert(p,pnew);

	}


	// remove duplicate points
	// snapping algorithm from P.O. Persson
	void DistMesh2D::remove_duplicate(){

		// check points
		if(!p_.is_finite())rat_throw_line("points contain nan or infinite");
		if(!qt_.is_finite())rat_throw_line("elements contain nan or infinite");

		// counters
		arma::uword num_points = p_.n_rows;
		arma::uword num_elements = qt_.n_rows;
		arma::uword num_sides = qt_.n_cols;

		// find typical size
		arma::Row<double>::fixed<2> delta = arma::max(p_,0)-arma::min(p_,0);
		double dp = arma::max(delta);
		double snap = dp*1024*arma::datum::eps;

		// snap points to integers
		arma::Mat<double> sp = arma::round(p_/snap)*snap;

		// sort rows
		arma::Col<arma::uword> original_index = arma::regspace<arma::Col<arma::uword> >(0,num_points-1);
		for(arma::uword i=0;i<sp.n_cols;i++){
			// create sort index
			const arma::Col<arma::uword> sort_index = arma::stable_sort_index(sp.col(i));
			
			// perform sorting 
			sp = sp.rows(sort_index); original_index = original_index.rows(sort_index);
		} 
		
		// find unique 
		arma::Col<arma::uword> connect = arma::regspace<arma::Col<arma::uword> >(0,num_points-1);
		arma::Col<arma::uword> duplicate(num_points,arma::fill::zeros);
		for(arma::uword i=1;i<sp.n_rows;i++){
			if(arma::all(sp.row(i)==sp.row(i-1))){
				connect(original_index(i)) = connect(original_index(i-1));
				duplicate(original_index(i)) = true;
			}
		}

		// shift indexes for node removal
		arma::Col<arma::uword> idx = arma::join_vert(
			arma::Row<arma::uword>{0},
			arma::cumsum(duplicate==false));
		connect = idx(connect);

		// Remove duplicate nodes 
		p_ = p_.rows(arma::find(duplicate==false));
		qt_ = arma::reshape(connect(arma::vectorise(qt_)),num_elements,num_sides);
	}

	// clockwise fix
	void DistMesh2D::fix_clockwise(){
		// count
		const arma::uword num_sides = qt_.n_cols;
		const arma::uword num_elements = qt_.n_rows;

		// check if elements are clockwise
		for(arma::uword i=0;i<num_elements;i++){
			// get coordinates
			arma::Mat<double> pe = p_.rows(qt_.row(i));

			// clockwise check
			double v = 0;
			for(arma::uword j=0;j<num_sides;j++){
				v += (pe((j+1)%num_sides,0) - pe(j,0))*(pe((j+1)%num_sides,1) + pe(j,1));
			}

			// fix element
			if(v<0){
				qt_.row(i) = arma::fliplr(qt_.row(i));
			}
		}
	}

	// create node graph
	void DistMesh2D::create_edges(){
		// check for empty
		if(qt_.is_empty())rat_throw_line("there is no elements");

		// count
		const arma::uword num_elements = qt_.n_rows;
		const arma::uword num_sides = qt_.n_cols;

		// allocate edges
		e_.set_size(num_elements*num_sides,2);

		// copy edges
		for(arma::uword i=0;i<num_sides;i++)
			e_.rows(i*num_elements,(i+1)*num_elements-1) = 
				qt_.cols(arma::Row<arma::uword>{i,(i+1)%num_sides});

		// sort columns
		e_ = arma::sort(e_,"ascend",1);

		// sort rows
		for(arma::uword i=0;i<e_.n_cols;i++){
			const arma::Col<arma::uword> sort_index = 
				arma::stable_sort_index(e_.col(i));
			e_ = e_.rows(sort_index);
		} 

		// find unique 
		arma::Col<arma::uword> is_unique(e_.n_rows,arma::fill::zeros); 
		is_unique(0) = 1; arma::uword last_unique = 0;
		for(arma::uword i=1;i<e_.n_rows;i++){
			if(arma::any(e_.row(i)!=e_.row(i-1))){
				is_unique(i)++;	last_unique = i;
			}else{
				is_unique(last_unique)++;
			}
		}

		// find surface edges
		es_ = e_.rows(arma::find(is_unique==1));

		// find internal edges
		ei_ = e_.rows(arma::find(is_unique>1));

		// select only unique rows
		e_ = e_.rows(arma::find(is_unique>0));

		// find surface node indexes
		ps_ = arma::unique(arma::vectorise(es_));

		// get only unique edges
		// e_ = cmn::Extra::unique_rows();
	}


	// recombination from triangles to quad
	void DistMesh2D::tri2quad(){
		// check tri mesh
		if(qt_.n_cols!=3)rat_throw_line("elements are not triangular");

		// get element graph
		arma::Col<arma::uword> row,col;
		element_graph(row,col,qt_);

		// allocate all possible quads
		arma::Mat<arma::uword> qnew(row.n_elem,4);

		// walk over combinations and calculate element quality
		for(arma::uword i=0;i<row.n_elem;i++){	
			// find common nodes
			const arma::Row<arma::uword>::fixed<3> t1 = qt_.row(row(i));
			const arma::Row<arma::uword>::fixed<3> t2 = qt_.row(col(i));

			// merge
			qnew.row(i) = merge2triangles(t1,t2);
		}

		// get quality for proposed elements
		arma::Col<double> Q;
		if(alternate_recombination_){
			Q = element2quality_alt(qnew,p_);
		}else{
			Q = element2quality(qnew,p_);
		}

		// greedy greedy algorithm ...
		// selects best quads for itself and 
		// leaves bad triangles for catmull clark subdivision
		// sort idexes. This could possibly be replaced with a perfect 
		// matching algorithm.
		arma::Col<arma::uword> srt = arma::sort_index(Q,"descend");

		// create original index
		arma::Col<arma::uword> original_index = arma::regspace<arma::Col<arma::uword> >(0,Q.n_rows-1);
		original_index = original_index.rows(srt);

		// keep track of triangles used
		arma::Col<arma::uword> triangle_used(qt_.n_rows,arma::fill::zeros);
		arma::Col<arma::uword> quad_used(qnew.n_rows,arma::fill::zeros);

		// select
		for(arma::uword i=0;i<row.n_elem;i++){
			// check quality
			if(Q(original_index(i))>recombination_treshold_){
				// get indexes
				arma::uword qidx = original_index(i);
				arma::uword tidx1 = row(qidx);
				arma::uword tidx2 = col(qidx);

				// check if both triangles still available
				if(triangle_used(tidx1)==false && triangle_used(tidx2)==false){
					// mark quad for creation
					quad_used(qidx) = true;
					triangle_used(tidx1) = true; 
					triangle_used(tidx2) = true;
				}
			}
		}

		// get new quads from list
		qnew = qnew.rows(arma::find(quad_used));

		// get unused triangles from list
		arma::Mat<arma::uword> tremain = qt_.rows(arma::find(triangle_used==false));

		// perform catmul clark on triangles and split rectangles
		arma::Mat<arma::uword> q1, q2; arma::Mat<double> p1, p2;
		refine(q1,p1,qnew,p_); refine(q2,p2,tremain,p_);

		// merge meshes
		qt_ = arma::join_vert(q1,q2+p1.n_rows);
		p_ = arma::join_vert(p1,p2);

		// remove diplicate nodes
		remove_duplicate();

		// fix clockwise
		fix_clockwise();

		// re-create edge list
		create_edges();

		// find snapping size
		double snap = arma::as_scalar(arma::max(arma::max(bbox_,0)-arma::min(bbox_,0),1))*1024*arma::datum::eps;

		// find points on surface that are not fixed
		arma::Col<arma::uword> is_unique(ps_.n_rows,arma::fill::ones);
		for(arma::uword i=0;i<pfix_.n_rows;i++){
			for(arma::uword j=0;j<ps_.n_rows;j++){
				is_unique(j) &= !arma::all(
					arma::round(pfix_.row(i)/snap)*snap == 
					arma::round(p_.row(ps_(j))/snap)*snap);
			}
		}
		arma::Col<arma::uword> psnf = ps_(arma::find(is_unique));

		// calculate d for surface points
		arma::Mat<double> ps = p_.rows(psnf);
		arma::Col<double> ds = fd_->calc_distance(ps);

		// calculate numerical gradient of the distance function
		arma::Mat<double> pdx = p_.rows(psnf); pdx.col(0)+=deps_;
		arma::Mat<double> pdy = p_.rows(psnf); pdy.col(1)+=deps_;
		arma::Col<double> dgradx = (fd_->calc_distance(pdx)-ds)/deps_;
		arma::Col<double> dgrady = (fd_->calc_distance(pdy)-ds)/deps_;
		arma::Col<double> dgrad2 = dgradx%dgradx + dgrady%dgrady;

		// Project points back to boundary
		p_.rows(psnf) -= arma::join_horiz(ds%dgradx/dgrad2, ds%dgrady/dgrad2);

		// smooth mesh
		smoothen();
	}

	// recombination from triangles to quad
	void DistMesh2D::smoothen(){
		// settings
		const arma::uword num_iter = 10;
		const double dmp = 0.7;

		// create neighbour list
		arma::Mat<arma::uword> nb = arma::join_vert(e_,arma::fliplr(e_));
		arma::Col<arma::uword> sort_index = arma::sort_index(nb.col(0));
		nb = nb.rows(sort_index);

		// find sections
		arma::Mat<arma::uword> sects = cmn::Extra::find_sections(nb.col(0).t());

		// iterations
		for(arma::uword j=0;j<num_iter;j++){
			// copy points list
			arma::Mat<double> pnew = p_;

			// walk over sections
			for(arma::uword i=0;i<sects.n_cols;i++){
				// node index
				arma::uword myidx = nb(sects(0,i),0);

				// find neighbours of this node
				arma::Col<arma::uword> mynb = nb.submat(sects(0,i),1,sects(1,i),1);

				// calculate new position
				pnew.row(myidx) = arma::mean(p_.rows(mynb),0);
			}

			// do not move surface elements
			pnew.rows(ps_) = p_.rows(ps_);

			// apply damping and move points
			p_ += dmp*(pnew-p_); 
		}
	}


	// set element size
	void DistMesh2D::set_h0(const double h0){
		h0_ = h0; 
		deps_ = std::sqrt(arma::datum::eps)*h0;
		geps_ = 0.001*h0;
	}

	// set quad mesh
	void DistMesh2D::set_quad(const bool quad){
		quad_ = quad;
	}

	// set distance function
	void DistMesh2D::set_distfun(ShDistFunPr fd){
		fd_ = fd;
	}

	// set scaling function
	void DistMesh2D::set_scalefun(ShDistFunPr fh){
		fh_ = fh;
	}

	// set maximum number of iterations
	void DistMesh2D::set_maxiter(const arma::uword maxiter){
		maxiter_ = maxiter;
	}

	// set fixed points
	void DistMesh2D::set_fixed(const arma::Mat<double> &pfix_ext){
		if(pfix_ext.n_cols!=2)rat_throw_line("fixed points must be supplied in 2 column matrix");
		pfix_ext_ = pfix_ext;
	}

	// set bounding box
	// void DistMesh2D::set_bounding(const arma::Mat<double> &bbox){
	// 	bbox_ = bbox;
	// }

	// get elements of mesh
	arma::Mat<arma::uword> DistMesh2D::get_elements() const{
		return qt_;
	}

	// get points of mesh
	arma::Mat<double> DistMesh2D::get_nodes() const{
		return p_;
	}

	// get surface edges
	arma::Mat<arma::uword> DistMesh2D::get_surface_edges() const{
		return es_;
	}

	// get surface edges
	arma::Mat<arma::uword> DistMesh2D::get_internal_edges() const{
		return ei_;
	}

	// get surface node indexes
	arma::Col<arma::uword> DistMesh2D::get_surface_node_idx() const{
		return ps_;
	}

	// distmesh setup function
	// translated from distmesh from Per Olof Persson
	// http://persson.berkeley.edu/distmesh/
	void DistMesh2D::setup(){
		// check input
		if(h0_<=0)rat_throw_line("element size must be larger than zero");
		if(pfix_ext_.n_cols!=2)rat_throw_line("fixed points must be stored in two columns x and y");

		// get bounding box
		bbox_ = fd_->get_bounding();

		// check bounding
		if(arma::any(bbox_.row(0)>=bbox_.row(1)))rat_throw_line("negatively or zero spaced bounding box");

		// extend bounding
		bbox_.row(0) -= 0.02*(bbox_.row(1) - bbox_.row(0));
		bbox_.row(1) += 0.02*(bbox_.row(1) - bbox_.row(0));

		// check if bounding box has any volume
		if(arma::prod(bbox_.row(1)-bbox_.row(0))<=0)rat_throw_line("bounding box inconsistent");

		// get fixed points
		pfix_ = arma::join_vert(fd_->get_fixed(),pfix_ext_);

		// check points
		if(pfix_.n_cols!=2)rat_throw_line("fixed points must be supplied in 2 column matrix");

		// check which fixed points are on the boundary and discard others
		pfix_ = pfix_.rows(arma::find(arma::abs(fd_->calc_distance(pfix_))<1e-9));

		// 1. create initial distribution in bounding box (equilateral triangles)
		// generate grid of coordinates
		arma::Row<double> xgv = arma::regspace<arma::Row<double> >(bbox_(0,0),h0_,bbox_(1,0));
		arma::Col<double> ygv = arma::regspace<arma::Col<double> >(bbox_(0,1),h0_*std::sqrt(3)/2,bbox_(1,1));
		arma::Mat<double> x,y;
		meshgrid(x,y,xgv,ygv);

		// shift even rows
		x.rows(arma::regspace<arma::Col<arma::uword> >(2,2,x.n_rows-1)) += h0_/2;

		// create list of node coordinates
		p_ = arma::join_horiz(arma::vectorise(x),arma::vectorise(y));

		// 2. remove points outside the region, apply the rejection method
		// evaluate distance function
		arma::Col<double> d = fd_->calc_distance(p_);

		// keep only d<0 points
		p_ = p_.rows(arma::find(d<geps_));

		// check if any point left
		if(p_.is_empty())rat_throw_line("shape function inconsistent");

		// probability to keep point
		arma::Col<double> r0 = 1.0/arma::pow(fh_->calc_distance(p_),2);
		
		// rejection method
		p_ = p_.rows(arma::find(arma::Col<double>(p_.n_rows,arma::fill::randu)<r0/arma::max(r0)));

		// find snapping size
		double snap = arma::as_scalar(arma::max(arma::max(bbox_,0)-arma::min(bbox_,0),1))*1024*arma::datum::eps;

		// Remove duplicated nodes
		arma::Col<arma::uword> is_unique(p_.n_rows,arma::fill::ones);
		for(arma::uword i=0;i<pfix_.n_rows;i++){
			for(arma::uword j=0;j<p_.n_rows;j++){
				is_unique(j) &= !arma::all(
					arma::round(pfix_.row(i)/snap)*snap == 
					arma::round(p_.row(j)/snap)*snap);
			}
		}
		p_ = p_.rows(arma::find(is_unique));
		arma::uword num_fix = pfix_.n_rows;

		// check number of points
		if(p_.is_empty())rat_throw_line("all points were removed");

		// prepend fix points
		p_ = arma::join_vert(pfix_,p_);

		// number of points N
		arma::uword N = p_.n_rows;

		// setup for first iteration
		arma::Mat<double> pold(N,2); pold.fill(arma::datum::inf);

		// iterate
		for(arma::uword k=0;k<maxiter_;k++){
			// check pold
			assert(!pold.is_empty());

			// count++;
			// 3. retriangulation by the Delaunay algorithm
			// Any large movement?
			//if(arma::as_scalar(arma::any(arma::sqrt(arma::sum(arma::pow(p_-pold,2),1))>ttol))){
			double delta = arma::as_scalar(arma::max(arma::sqrt(arma::sum(arma::pow(p_-pold,2),1))/h0_));
			if(delta>ttol_){
				// save current positions
				pold = p_;                                          
				
				// create list of triangles
				qt_ = triangulation(p_); 
				
				// Compute centroids
				arma::Mat<double> pmid = (p_.rows(qt_.col(0)) + p_.rows(qt_.col(1)) + p_.rows(qt_.col(2)))/3;
			
				// Keep interior triangles only
				qt_ = qt_.rows(arma::find(fd_->calc_distance(pmid)<(-geps_)));
				// arma::uword num_tri = qt_.n_rows;
				if(qt_.n_rows==0)rat_throw_line("no triangles left inside mesh");

				// create edge list
				create_edges();

				// // 4. Describe each bar by a unique pair of nodes
				// // Interior bars duplicated
				// bars.set_size(3*num_tri,2);
				// bars.rows(0*num_tri,1*num_tri-1) = qt_.cols(arma::Row<arma::uword>{0,1});
				// bars.rows(1*num_tri,2*num_tri-1) = qt_.cols(arma::Row<arma::uword>{0,2});
				// bars.rows(2*num_tri,3*num_tri-1) = qt_.cols(arma::Row<arma::uword>{1,2});

				// // get only unique edges
				// bars = cmn::Extra::unique_rows(arma::sort(bars,"ascend",1));
			}

			// 6. Move mesh points based on bar lengths L and forces F
			// List of bar vectors
			const arma::Mat<double> evec = p_.rows(e_.col(0))-p_.rows(e_.col(1));

			// L = Bar lengths
			arma::Col<double> L = arma::sqrt(arma::sum(evec%evec,1));

			// Scaling function
			arma::Mat<double> pbar = (p_.rows(e_.col(0))+p_.rows(e_.col(1)))/2;
			arma::Col<double> he = fh_->calc_distance(pbar);

			// L0 = Desired lengths
			arma::Col<double> L0 = he*Fscale_*std::sqrt(arma::sum(L%L)/arma::sum(he%he));     
		
			// Density control - remove points that are too close
			if((k+1)%densityctrlfreq_==0 && arma::any(L0>2*L)){
				// std::cout<<"controlling density"<<std::endl;
				// get points to remove
				arma::Col<arma::uword> idx_remove = arma::vectorise(e_.rows(arma::find(L0>2*L)));
				
				// do not touch fixed
				idx_remove = idx_remove.rows(arma::find(idx_remove>=num_fix));

				// remove rows from p
				arma::Col<arma::uword> idx_keep(N,arma::fill::ones);
				idx_keep.rows(idx_remove).fill(0);
				p_ = p_.rows(arma::find(idx_keep));

				// update counter
				N = p_.n_rows;	

				// ensure that the mesh is re-setup	
				pold.set_size(N,2);	pold.fill(arma::datum::inf);

				// check if there are points remaining
				if(p_.is_empty())rat_throw_line("all points were removed during density control");

				// restart
				continue;
			}

			// calculate bar forces (scalars)
			arma::Col<double> F = arma::clamp(L0-L,0,arma::datum::inf); 
			
			// Bar forces (x,y components)
			// arma::Mat<double> Fvec = (F/L)%evec.each_col();
			arma::Mat<double> Fvec = F/L*arma::Row<double>{1,1}%evec;

			// Force on points
			arma::Mat<double> Ftot(N,2,arma::fill::zeros);
			for(arma::uword j=0;j<e_.n_rows;j++){
				Ftot.row(e_(j,0)) += Fvec.row(j);
				Ftot.row(e_(j,1)) -= Fvec.row(j);
			}
			// Ftot=full(sparse(bars(:,[1,1,2,2]),ones(size(F))*[1,2,1,2],[Fvec,-Fvec],N,2));
			
			// Force = 0 at fixed points
			if(num_fix>0)Ftot.rows(0,num_fix-1).fill(0); 
			
			// Update node positions
			p_ += deltat_*Ftot;

			// 7. Bring outside points back to the boundary
			// Find points outside (d>0)
			d=fd_->calc_distance(p_); assert(!d.is_empty());
			arma::Col<arma::uword> idx_outside = arma::find(d>0);

			// check if any internal points left
			assert(arma::any(d<(-geps_)));

			// calculate numerical gradient of the distance function
			arma::Mat<double> pdx = p_.rows(idx_outside); pdx.col(0)+=deps_;
			arma::Mat<double> pdy = p_.rows(idx_outside); pdy.col(1)+=deps_;
			arma::Col<double> dgradx = (fd_->calc_distance(pdx)-d.rows(idx_outside))/deps_;
			arma::Col<double> dgrady = (fd_->calc_distance(pdy)-d.rows(idx_outside))/deps_;
			arma::Col<double> dgrad2 = dgradx%dgradx + dgrady%dgrady;

			// Project points back to boundary
			p_.rows(idx_outside) -= arma::join_horiz(d(idx_outside)%dgradx/dgrad2, d(idx_outside)%dgrady/dgrad2);

			// 8. Termination criterion: All interior nodes move less than dptol (scaled)
			const arma::Col<arma::uword> idx = arma::find(d<(-geps_));
			if(!idx.is_empty()){
				double conv = arma::as_scalar(arma::max(arma::sqrt(arma::sum(
					deltat_*arma::square(Ftot.rows(idx)),1))/h0_));

				// std::cout<<conv<<" "<<dptol<<std::endl;
				if(conv<dptol_ && k!=0)break;
			}

			// if max(sqrt(sum(deltat*Ftot(d<-geps,:).^2,2))/h0)<dptol, break; end
			//max(sqrt(sum(deltat*Ftot(d<-geps,:).^2,2))/h0)<dptol, break; end
		}
		
		// remove diplicate nodes (if any)
		remove_duplicate();

		// fix clockwise (if needed)
		fix_clockwise();

		// Quad mesh recombination
		if(quad_)tri2quad();
	}

	// gmsh export
	void DistMesh2D::export_gmsh(cmn::ShGmshFilePr gmsh) const{
		// write points in xy plane with z=0
		gmsh->write_nodes(arma::join_horiz(p_,arma::Col<double>(p_.n_rows,arma::fill::zeros)).t());

		// write elements
		gmsh->write_elements(qt_.t());
	}


	// function for calculating area
	double DistMesh2D::calc_area() const{
		// get counters
		const arma::uword num_elements = qt_.n_rows;
		const arma::uword num_edges = qt_.n_cols;

		// allocate areas
		arma::Col<double> A(num_elements,arma::fill::zeros);

		// calculate areas
		for(arma::uword i=0;i<num_elements;i++){
			// get points
			const arma::Mat<double> pt = p_.rows(qt_.row(i));

			// walk over triangles or quadrangles
			for(arma::uword j=0;j<num_edges-2;j++){
				// get two edges
				const arma::Row<double>::fixed<2> a = pt.row((2*j+1)%num_edges) - pt.row((2*j)%num_edges);
				const arma::Row<double>::fixed<2> b = pt.row((2*j+2)%num_edges) - pt.row((2*j+1)%num_edges);

				// calculate area using cross product
				A(i) += std::abs(a(0)*b(1) - a(1)*b(0))/2;
			}
		}

		// sum areas and return
		return arma::sum(A);
	}

	// type string for serialization
	std::string DistMesh2D::get_type(){
		return "rat::dm::dfdistmesh2d";
	}

	// method for serialization into json
	void DistMesh2D::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// quad mesher enabled
		js["quad"] = quad_;

		// element size
		js["h0"] = h0_;
		
		// externally supplied fixed point list
		for(arma::uword i=0;i<pfix_ext_.n_rows;i++){
			js["pfix_x"].append(pfix_ext_(i,0));
			js["pfix_y"].append(pfix_ext_(i,1));
		}

		// distance functions
		js["geometryfun"] = cmn::Node::serialize_node(fd_,list);
		js["scalingfun"] = cmn::Node::serialize_node(fh_,list);
	}

	// method for deserialisation from json
	void DistMesh2D::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
	
		// quad mesher enabled 
		set_quad(js["quad"].asBool());

		// element size
		set_h0(js["h0"].asDouble());

		// externally supplied fixed point list
		pfix_ext_.set_size(js["pfix_x"].size(),2);
		Json::Value x = js["pfix_x"]; Json::Value y = js["pfix_y"];
		for(arma::uword i=0;i<x.size();i++){
			pfix_ext_(i,0) = x.get(i,0).asDouble(); 
			pfix_ext_(i,1) = y.get(i,0).asDouble();
		}
		
		// distance functions
		fd_ = cmn::Node::deserialize_node<DistFun>(js["geometryfun"], list, factory_list, pth);
		fh_ = cmn::Node::deserialize_node<DistFun>(js["scalingfun"], list, factory_list, pth);
	}

	// // custom function for finding sections in array
	// // for example find_sections([1,1,1,2,2,2,3,3],"first") should give [0,3,6]
	// // for example find_sections([1,1,1,2,2,2,3,3],"last") should give [2,5,7]
	// arma::Mat<arma::uword> DistMesh2D::find_sections(const arma::Row<arma::uword> &M){
	// 	// make sure it is a row vector
	// 	assert(M.n_rows==1);

	// 	// allocate output
	// 	arma::Mat<arma::uword> indices;

	// 	// check if matrix is empty
	// 	if(!M.is_empty()){
	// 		// find indices at changing parts
	// 		arma::Row<arma::uword> idx = arma::find(M.tail_cols(M.n_cols-1)!=M.head_cols(M.n_cols-1)).t(); 
		
	// 		// make first and last indexes
	// 		arma::Row<arma::uword> idx1 = arma::join_horiz(arma::Mat<arma::uword>(1,1,arma::fill::zeros),idx+1);
	// 		arma::Row<arma::uword> idx2 = arma::join_horiz(idx,arma::Mat<arma::uword>(1,1,arma::fill::ones)*(M.n_cols-1));
		
	// 		// return
	// 		indices = arma::join_vert(idx1,idx2);
	// 	}

	// 	// if matrix is empty return empty indices
	// 	else{
	// 		indices = arma::Mat<arma::uword>(2,0);
	// 	}

	// 	// return output matrix with indexes
	// 	return indices;
	// }

}}
