/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfdiff.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFDiff::DFDiff(ShDistFunPr dfa, ShDistFunPr dfb){
		// check user input
		if(dfa==NULL)rat_throw_line("first distance function points to NULL");
		if(dfb==NULL)rat_throw_line("second distance function points to NULL");

		// set to self
		dfa_ = dfa; dfb_ = dfb;
	}

	// factory
	ShDFDiffPr DFDiff::create(ShDistFunPr dfa, ShDistFunPr dfb){
		return std::make_shared<DFDiff>(dfa,dfb);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFDiff::get_bounding() const{
		return dfa_->get_bounding(); // only A as B is subtracted
	}

	// get fixed points
	arma::Mat<double> DFDiff::get_fixed() const{
		return arma::join_vert(dfa_->get_fixed(),dfb_->get_fixed());
	}

	// distance function
	arma::Col<double> DFDiff::calc_distance(const arma::Mat<double> &p) const{
		return arma::max(dfa_->calc_distance(p),-dfb_->calc_distance(p));
	}

	// type string for serialization
	std::string DFDiff::get_type(){
		return "rat::dm::dfdiff";
	}

	// method for serialization into json
	void DFDiff::serialize(Json::Value &js, cmn::SList &list) const{
		// store type
		js["type"] = get_type();

		// serialize distance functions
		js["distfuna"] = cmn::Node::serialize_node(dfa_,list);
		js["distfunb"] = cmn::Node::serialize_node(dfb_,list);
	}

	// method for deserialisation from json
	void DFDiff::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// deserialize distance functions
		dfa_ = cmn::Node::deserialize_node<DistFun>(js["distfuna"], list, factory_list, pth);
		dfb_ = cmn::Node::deserialize_node<DistFun>(js["distfunb"], list, factory_list, pth);
	}

}}