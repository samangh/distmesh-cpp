/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfunion.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFUnion::DFUnion(ShDistFunPr dfa, ShDistFunPr dfb){
		if(dfa==NULL)rat_throw_line("first distance function points to NULL");
		if(dfb==NULL)rat_throw_line("second distance function points to NULL");
		df_.set_size(2); df_(0) = dfa; df_(1) = dfb;
	}

	// factory
	ShDFUnionPr DFUnion::create(){
		return std::make_shared<DFUnion>();
	}

	// factory
	ShDFUnionPr DFUnion::create(ShDistFunPr dfa, ShDistFunPr dfb){
		return std::make_shared<DFUnion>(dfa,dfb);
	}

	// add parts
	void DFUnion::add(ShDistFunPr df){
		// check input
		if(df==NULL)rat_throw_line("distance function points to NULL");

		// allocate new source list
		arma::field<ShDistFunPr> new_df(df_.n_elem + 1);

		// set old and new sources
		for(arma::uword i=0;i<df_.n_elem;i++)new_df(i) = df_(i);
		new_df(df_.n_elem) = df;

		// set new source list
		df_ = new_df;
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFUnion::get_bounding() const{
		// check input
		if(df_.is_empty())rat_throw_line("distance functions not set");

		// get boxes of both
		arma::Mat<double>::fixed<2,2> M = df_(0)->get_bounding();

		// take overlapping area between two boxes
		if(df_.n_elem>1){
			for(arma::uword i=1;i<df_.n_elem;i++){
				const arma::Mat<double>::fixed<2,2> Mt = df_(i)->get_bounding();
				M.row(0) = arma::min(M.row(0),Mt.row(0));
				M.row(1) = arma::max(M.row(1),Mt.row(1));
			}
		}

		// return box
		return M;
	}

	// get fixed points
	arma::Mat<double> DFUnion::get_fixed() const{
		// check input
		if(df_.is_empty())rat_throw_line("distance functions not set");

		// allocate
		arma::field<arma::Mat<double> > pfixfld(df_.n_elem);

		// get fixed points
		arma::uword num_points = 0;
		for(arma::uword i=0;i<df_.n_elem;i++){
			pfixfld(i) = df_(i)->get_fixed();
			assert(pfixfld(i).n_cols==2);
			num_points+= pfixfld(i).n_rows;
		}

		// combine fixed points
		arma::Mat<double> pfix(num_points,2);

		// store in matrix
		arma::uword cnt = 0;
		for(arma::uword i=0;i<pfix.n_elem;i++){
			const arma::uword num_store = pfixfld(i).n_rows;
			pfix.rows(cnt,cnt+num_store-1) = pfixfld(i);
			cnt+=num_store;
		}

		// output point list
		return pfix;
	}

	// distance function
	arma::Col<double> DFUnion::calc_distance(const arma::Mat<double> &p) const{
		// check if not empty
		if(df_.is_empty())rat_throw_line("distance functions not set");

		// calc distances to all shapes
		arma::Mat<double> v(p.n_rows, df_.n_elem);
		for(arma::uword i=0;i<df_.n_elem;i++)
			v.col(i) = df_(i)->calc_distance(p);

		// return smallest one
		return arma::min(v,1);
	}

	// type string for serialization
	std::string DFUnion::get_type(){
		return "rat::dm::dfunion";
	}

	// method for serialization into json
	void DFUnion::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		
		for(arma::uword i=0;i<df_.n_elem;i++){
		 	js["distfuns"].append(cmn::Node::serialize_node(df_(i),list));
		}
	}

	// method for deserialisation from json
	void DFUnion::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// get number of distance functions
		const arma::uword num_distfuns = js["distfuns"].size();
		
		// allocate
		df_.set_size(num_distfuns);

		// create distance functions
		for(arma::uword i=0;i<num_distfuns;i++){
			df_(i) = cmn::Node::deserialize_node<DistFun>(js["distfuns"].get(i,0), list, factory_list, pth);
		}
	}

}}