/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfpolygon.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructor
	DFPolygon::DFPolygon(const arma::Mat<double> &v){
		// check user input
		if(v.is_empty())rat_throw_line("polygon coordinate matrix is empty");
		if(!v.is_finite())rat_throw_line("polygon coordinate matrix must be finite");
		if(v.n_cols!=2)rat_throw_line("polygon coordinate matrix must have two columns");

		// set to self
		v_ = v;
	}

	// factory
	ShDFPolygonPr DFPolygon::create(const arma::Mat<double> &v){
		return std::make_shared<DFPolygon>(v);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFPolygon::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{arma::min(v_.col(0)),arma::max(v_.col(0))};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{arma::min(v_.col(1)),arma::max(v_.col(1))};

		// return box
		return Mb;
	}


	// get fixed points
	arma::Mat<double> DFPolygon::get_fixed() const{
		return v_;
	}


	// point in polygon check
	arma::Col<arma::uword> DFPolygon::inpolygon(
		const arma::Mat<double> &v, const arma::Mat<double> &p){
		// get counters
		const arma::uword nv = v.n_rows;
		const arma::uword np = p.n_rows;

		// allocate checklist
		arma::Col<arma::uword> chk(np,arma::fill::zeros);

		// walk over points
		for(arma::uword k=0;k<np;k++){
			// walk over edges
			for(arma::uword i=0,j=nv-1;i<nv;j=i++){
				if(((v(i,1)>p(k,1))!=(v(j,1)>p(k,1))) && (p(k,0)<(v(j,0)-v(i,0))*(p(k,1)-v(i,1))/(v(j,1)-v(i,1))+v(i,0)))
					chk(k) = !chk(k);
			}
		}

		// return list
		return chk;
	}


	// distance function
	arma::Col<double> DFPolygon::calc_distance(const arma::Mat<double> &p) const{
		// number of points and number of sections
		arma::uword np = p.n_rows;
		arma::uword nv = v_.n_rows;

		// distance matrix
		arma::Mat<double> ds(np,nv);

		// walk over sections
		for(arma::uword i=0,j=nv-1;i<nv;j=i++){
			// get vertices
			const arma::Row<double>::fixed<2> v1 = v_.row(i);
			const arma::Row<double>::fixed<2> v2 = v_.row(j);
				
			// calculate edge vector
			const arma::Row<double>::fixed<2> dv = v2-v1;

			// walk over points
			for(arma::uword ip=0;ip<np;ip++){
				// get point
				const arma::Row<double>::fixed<2> myp = p.row(ip);

				// calculate vector from first vertex to point
				const arma::Row<double>::fixed<2> dp = myp - v1;

				// calculate dot products
				const double c1 = arma::accu(dv%dp);
				const double c2 = arma::accu(dv%dv);
				
				// distance to projected point on line
				ds(ip,i) = std::sqrt(arma::accu(arma::square(
					myp-(v1+std::min(1.0,std::max(0.0,c1/c2))*dv))));
			}
		}

		// check which points are inside the polygon
		const arma::Col<double> sign = 2*arma::conv_to<arma::Col<double> >::from(inpolygon(v_,p)) - 1;

		// check output
		assert(sign.is_finite());

		// signed distance
		arma::Col<double> sds = -arma::min(ds,1)%sign;

		// negative distance inside
		return sds;
	}

	// type string for serialization
	std::string DFPolygon::get_type(){
		return "rat::dm::dfpolygon";
	}

	// method for serialization into json
	void DFPolygon::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// store type ID
		js["type"] = get_type();
		for(arma::uword i=0;i<v_.n_rows;i++){
			js["x"].append(v_(i,0));
			js["y"].append(v_(i,1));
		}
	}

	// method for deserialisation from json
	void DFPolygon::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// get values
		v_.set_size(js["x"].size(),2);
		const Json::Value x = js["x"]; const Json::Value y = js["y"];
		for(arma::uword i=0;i<x.size();i++){
			v_(i,0) = x.get(i,0).asDouble(); 
			v_(i,1) = y.get(i,0).asDouble();
		}
	}

}}