/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfellipse.hh"

// code specific to Rat
namespace rat{namespace dm{
	
	// constructcor
	DFEllipse::DFEllipse(const double a, const double b, const double xe, const double ye){
		// check user input
		if(a<=0)rat_throw_line("a must be larger than zero");
		if(b<=0)rat_throw_line("b must be larger than zero");

		// set to self
		a_ = a; b_ = b; xe_ = xe; ye_ = ye;
	}

	// factory
	ShDFEllipsePr DFEllipse::create(const double a, const double b, const double xe, const double ye){
		return std::make_shared<DFEllipse>(a,b,xe,ye);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFEllipse::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{xe_-a_/2,xe_+a_/2};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{ye_-b_/2,ye_+b_/2};

		// return box
		return Mb;
	}

	// get fixed points
	arma::Mat<double> DFEllipse::get_fixed() const{
		// no fixed points
		return arma::Mat<double>(0,2);
	}

	// distance function
	arma::Col<double> DFEllipse::calc_distance(const arma::Mat<double> &p) const{
		return (p.col(0)-xe_)%(p.col(0)-xe_)/(a_*a_) + (p.col(1)-ye_)%(p.col(1)-ye_)/(b_*b_)-1;
	}

	// type string for serialization
	std::string DFEllipse::get_type(){
		return "rat::dm::dfellipse";
	}

	// method for serialization into json
	void DFEllipse::serialize(Json::Value &js, cmn::SList &) const{
		// store type ID
		js["type"] = get_type();
		js["a"] = a_; js["b"] = b_;
		js["xe"] = xe_; js["ye"] = ye_;
	}

	// method for deserialisation from json
	void DFEllipse::deserialize(
		const Json::Value &js, cmn::DSList &, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// get values
		a_ = js["a"].asDouble(); b_ = js["b"].asDouble();
		xe_ = js["xe"].asDouble(); ye_ = js["ye"].asDouble();
	}

}}