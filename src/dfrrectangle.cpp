/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfrrectangle.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFRRectangle::DFRRectangle(const double x1, const double x2, const double y1, const double y2, const double radius){
		// check input
		if(x2<=x1)rat_throw_line("x2 must be larger than x1");
		if(y2<=y1)rat_throw_line("y2 must be larger than y1");
		if(radius<=0)rat_throw_line("radius must be larger than zero");

		// set to self
		x1_ = x1; x2_ = x2; y1_ = y1; y2_ = y2; radius_ = radius;
	}

	// factory
	ShDFRRectanglePr DFRRectangle::create(const double x1, const double x2, const double y1, const double y2, const double radius){
		return std::make_shared<DFRRectangle>(x1,x2,y1,y2,radius);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFRRectangle::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{x1_,x2_};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{y1_,y2_};

		// return box
		return Mb;
	}

	// get fixed points
	arma::Mat<double> DFRRectangle::get_fixed() const{
		// create list of fixed points
		arma::Mat<double> pfix(8,2);
		pfix.row(0) = arma::Row<double>{x1_+radius_,y1_};
		pfix.row(1) = arma::Row<double>{x1_,y1_+radius_};
		pfix.row(2) = arma::Row<double>{x1_+radius_,y2_};
		pfix.row(3) = arma::Row<double>{x1_,y2_-radius_};
		pfix.row(4) = arma::Row<double>{x2_-radius_,y1_};
		pfix.row(5) = arma::Row<double>{x2_,y1_+radius_};
		pfix.row(6) = arma::Row<double>{x2_-radius_,y2_};
		pfix.row(7) = arma::Row<double>{x2_,y2_-radius_};
		return pfix;
	}

	// distance function
	arma::Col<double> DFRRectangle::calc_distance(const arma::Mat<double> &p) const{
		// create union
		ShDFUnionPr df = DFUnion::create();

		// add rectangles
		df->add(DFRectangle::create(x1_,x2_,y1_+radius_,y2_-radius_));
		df->add(DFRectangle::create(x1_+radius_,x2_-radius_,y1_,y2_));

		// add circles for corners
		df->add(DFCircle::create(radius_,x1_+radius_,y1_+radius_));
		df->add(DFCircle::create(radius_,x2_-radius_,y1_+radius_));
		df->add(DFCircle::create(radius_,x1_+radius_,y2_-radius_));
		df->add(DFCircle::create(radius_,x2_-radius_,y2_-radius_));

		// return distance
		return df->calc_distance(p);
	}

	// type string for serialization
	std::string DFRRectangle::get_type(){
		return "rat::dm::dfrrectangle";
	}


	// method for serialization into json
	void DFRRectangle::serialize(Json::Value &js, cmn::SList &) const{
		// store type ID
		js["type"] = get_type();
		js["x1"] = x1_; js["x2"] = x2_;
		js["y1"] = y1_;	js["y2"] = y2_;
		js["radius"] = radius_;
	}

	// method for deserialisation from json
	void DFRRectangle::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// get values
		x1_ = js["x1"].asDouble(); x2_ = js["x2"].asDouble();
		y1_ = js["y1"].asDouble(); y2_ = js["y2"].asDouble();
		radius_ = js["radius"].asDouble();
	}

}}