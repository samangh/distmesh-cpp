/* Raccoon - An Electro-Magnetic and Thermal network 
solver accelerated with the Multi-Level Fast Multipole Method
Copyright (C) 2020  Jeroen van Nugteren*/

// include header
#include "serializer.hh"

// serializeable objects
#include "distmesh2d.hh"
#include "dfairfoil.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"
#include "dfellipse.hh"
#include "dfintersect.hh"
#include "dfones.hh"
#include "dfpolygon.hh"
#include "dfrectangle.hh"
#include "dfrrectangle.hh"
#include "dfscale.hh"
#include "dfunion.hh"

// code specific to Raccoon
namespace rat{namespace dm{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(const boost::filesystem::path &fname){
		register_constructors();
		import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// register all required constructors
		register_factory<DistMesh2D>();
		register_factory<DFAirFoil>();
		register_factory<DFCircle>();
		register_factory<DFDiff>();
		register_factory<DFEllipse>();
		register_factory<DFIntersect>();
		register_factory<DFOnes>();
		register_factory<DFPolygon>();
		register_factory<DFRectangle>();
		register_factory<DFRRectangle>();
		register_factory<DFScale>();
		register_factory<DFUnion>();
	}

}}