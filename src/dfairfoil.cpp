
/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfairfoil.hh"

// code specific to Rat
namespace rat{namespace dm{

	// factory
	ShDFAirFoilPr DFAirFoil::create(){
		return std::make_shared<DFAirFoil>();
	}

	// set values
	void DFAirFoil::set_a(const arma::Col<double> &a){
		if(a.n_elem!=5)rat_throw_line("a must have five values");
		a_ = a;
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFAirFoil::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{0,1};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{-0.1,0.1};

		// return box
		return Mb;
	}

	// get fixed points
	arma::Mat<double> DFAirFoil::get_fixed() const{
		return arma::Row<double>{1,0};
	}

	// distance function
	arma::Col<double> DFAirFoil::calc_distance(const arma::Mat<double> &p) const{
		return arma::pow(arma::abs(p.col(1))-arma::polyval(arma::join_vert(
			a_.rows(arma::regspace<arma::Col<arma::uword> >(4,1)),
			arma::Col<double>(1,arma::fill::zeros)),
			p.col(0)),2)-std::pow(a_(0),2)*p.col(0);
	}

	// type string for serialization
	std::string DFAirFoil::get_type(){
		return "rat::dm::dfairfoil";
	}
	
	// method for serialization into json
	void DFAirFoil::serialize(Json::Value &js, cmn::SList &) const{
			// store type ID
		js["type"] = get_type();
		js["a0"] = a_(0); js["a1"] = a_(1); js["a2"] = a_(2);
		js["a3"] = a_(3); js["a4"] = a_(4);	
	}

	// method for deserialisation from json
	void DFAirFoil::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// get values
		a_.set_size(5);
		a_(0) = js["a0"].asDouble(); a_(1) = js["a1"].asDouble();
		a_(2) = js["a2"].asDouble(); a_(3) = js["a3"].asDouble(); 
		a_(4) = js["a4"].asDouble();
	}
	
}}