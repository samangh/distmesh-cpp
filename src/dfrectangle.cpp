/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfrectangle.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFRectangle::DFRectangle(const double x1, const double x2, const double y1, const double y2){
		// check user input
		if(x2<=x1)rat_throw_line("x2 must be larger than x1");
		if(y2<=y1)rat_throw_line("y2 must be larger than y1");

		// set to self
		x1_ = x1; x2_ = x2; y1_ = y1; y2_ = y2; 
	}

	// factory
	ShDFRectanglePr DFRectangle::create(const double x1, const double x2, const double y1, const double y2){
		return std::make_shared<DFRectangle>(x1,x2,y1,y2);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFRectangle::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{x1_,x2_};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{y1_,y2_};

		// return box
		return Mb;
	}


	// get fixed points
	arma::Mat<double> DFRectangle::get_fixed() const{
		// create list of fixed points
		arma::Mat<double> pfix(4,2);
		pfix.row(0) = arma::Row<double>{x1_,y1_};
		pfix.row(1) = arma::Row<double>{x1_,y2_};
		pfix.row(2) = arma::Row<double>{x2_,y1_};
		pfix.row(3) = arma::Row<double>{x2_,y2_};
		return pfix;
	}

	// distance function
	arma::Col<double> DFRectangle::calc_distance(const arma::Mat<double> &p) const{
		return -arma::min(arma::min(arma::min(-y1_+p.col(1),y2_-p.col(1)),-x1_+p.col(0)),x2_-p.col(0));
	}

	// type string for serialization
	std::string DFRectangle::get_type(){
		return "rat::dm::dfrectangle";
	}

	// method for serialization into json
	void DFRectangle::serialize(Json::Value &js, cmn::SList &) const{
		// store type ID
		js["type"] = get_type();
		js["x1"] = x1_; js["x2"] = x2_;
		js["y1"] = y1_;	js["y2"] = y2_;
	}

	// method for deserialisation from json
	void DFRectangle::deserialize(
		const Json::Value &js, cmn::DSList &, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// get values
		x1_ = js["x1"].asDouble(); x2_ = js["x2"].asDouble();
		y1_ = js["y1"].asDouble(); y2_ = js["y2"].asDouble();
	}

}}