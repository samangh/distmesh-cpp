/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "dfcircle.hh"

// code specific to Rat
namespace rat{namespace dm{

	// constructcor
	DFCircle::DFCircle(const double radius, const double xc, const double yc){
		if(radius<0)rat_throw_line("radius must be equal to or larger than zero");
		radius_ = radius; xc_ = xc; yc_ = yc;
	}

	

	// factory
	ShDFCirclePr DFCircle::create(const double radius, const double xc, const double yc){
		return std::make_shared<DFCircle>(radius,xc,yc);
	}

	// get bounding box
	arma::Mat<double>::fixed<2,2> DFCircle::get_bounding() const{
		// allocate
		arma::Mat<double>::fixed<2,2> Mb;
		
		// bounding in x
		Mb.col(0) = arma::Col<double>::fixed<2>{xc_-radius_,xc_+radius_};
		
		// bounding in y
		Mb.col(1) = arma::Col<double>::fixed<2>{yc_-radius_,yc_+radius_};

		// return box
		return Mb;
	}

	// get fixed points
	arma::Mat<double> DFCircle::get_fixed() const{
		// no fixed points
		return arma::Mat<double>(0,2);
	}

	// distance function
	arma::Col<double> DFCircle::calc_distance(const arma::Mat<double> &p) const{
		return arma::sqrt(arma::pow(p.col(0)-xc_,2)+arma::pow(p.col(1)-yc_,2))-radius_;
	}

	// perimeter function
	// ShPerimeterPr DFCircle::get_perimeter() const{
	// 	// estimate element size from bounding box
	// 	const arma::Mat<double>::fixed<2,2> M = get_bounding();
	// 	const arma::Row<double>::fixed<2> size = M.row(1) - M.row(0);

	// 	// element size
	// 	const double dl = arma::max(size)/64;

	// 	// calculate number of elements
	// 	const arma::uword num_elements = std::ceil(2*arma::datum::pi*radius_/dl);

	// 	// create node coordinates on a circle
	// 	const arma::Row<double> theta = arma::linspace<arma::Row<double> >(
	// 		0,(((double)num_elements-1.0)/num_elements)*2*arma::datum::pi,num_elements);
	// 	const arma::Row<double> rho = arma::Row<double>(num_elements,arma::fill::ones)*radius_;
	// 	const arma::Mat<double> Rn = arma::join_vert(rho%arma::sin(theta), rho%arma::cos(theta));

	// 	// create elements
	// 	const arma::Mat<arma::uword> n = arma::join_vert(
	// 		arma::regspace<arma::Row<arma::uword> >(0,num_elements-2),
	// 		arma::regspace<arma::Row<arma::uword> >(1,num_elements-1));

	// 	// create perimeter
	// 	return Perimeter::create(Rn,n);
	// }

	// type string for serialization
	std::string DFCircle::get_type(){
		return "rat::dm::dfcircle";
	}

	// method for serialization into json
	void DFCircle::serialize(Json::Value &js, cmn::SList &) const{
		// store type ID
		js["type"] = get_type();
		js["xc"] = xc_;	js["yc"] = yc_;
		js["radius"] = radius_;
	}

	// method for deserialisation from json
	void DFCircle::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/, 
		const boost::filesystem::path &/*pth*/){
		// store type ID
		xc_ = js["xc"].asDouble(); 
		yc_ = js["yc"].asDouble();
		radius_ = js["radius"].asDouble();
	}

}}