/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_DF_RRECTANGLE_HH
#define DM_DF_RRECTANGLE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "distfun.hh"
#include "dfrectangle.hh"
#include "dfcircle.hh"
#include "dfunion.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFRRectangle> ShDFRRectanglePr;

	// rounded rectangle distance function
	class DFRRectangle: public DistFun{
		// properties
		private:
			// radius
			double radius_;

			// boundaries
			double x1_;
			double x2_;
			double y1_;
			double y2_;

		// methods
		public:
			// constructcor
			DFRRectangle(){};
			DFRRectangle(const double x1, const double x2, const double y1, const double y2, const double radius);

			// factory
			static ShDFRRectanglePr create(const double x1, const double x2, const double y1, const double y2, const double radius);

			// fixed points
			virtual arma::Mat<double> get_fixed() const override;

			// get bounding box
			virtual arma::Mat<double>::fixed<2,2> get_bounding() const override;

			// distance function
			virtual arma::Col<double> calc_distance(const arma::Mat<double> &p) const override;

			// serialization
			static std::string get_type(); 
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
