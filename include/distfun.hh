/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_DISTFUN_HH
#define DM_DISTFUN_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>
#include <algorithm>
#include <list>

#include "rat/common/defines.hh"
#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DistFun> ShDistFunPr;

	// circle distance function for distmesh
	class DistFun: public cmn::Node{
		// methods
		public:
			// virtual destructor
			virtual ~DistFun(){};

			// get bounding
			virtual arma::Mat<double>::fixed<2,2> get_bounding() const = 0;

			// fixed points
			virtual arma::Mat<double> get_fixed() const = 0;

			// distance function
			virtual arma::Col<double> calc_distance(const arma::Mat<double> &p) const = 0;

			// polymorphic factory and serialization
			static ShDistFunPr create(const Json::Value &js);
	};

}}

#endif
