/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_DISTMESH2D_HH
#define DM_DISTMESH2D_HH

// general headers
#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

// fx-common headers
#include "rat/common/extra.hh"
#include "rat/common/defines.hh"

// distmesh-cpp headers
#include "rat/common/gmshfile.hh"
#include "rat/common/node.hh"
#include "dfones.hh"
#include "distfun.hh"


// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DistMesh2D> ShDistMesh2DPr;

	// a lightweight tri and quad mesh generator
	// based on the 2D distmesh from Per Olof Persson
	// http://persson.berkeley.edu/distmesh/
	// Per-Olof Persson and Gilbert Strang, "A Simple Mesh 
	// Generator in MATLAB," SIAM Review Vol. 46 (2) 2004.
	// using delaunay triangulation algorithm from
	// Paul Bourke http://paulbourke.net/papers/triangulate/
	// both algorithms have been translated to Armadillo style
	// programming
	class DistMesh2D: public cmn::Node{
		// input parameters
		private:
			// tri-mesh or quad mesh
			bool quad_ = true; // default when used in Rat

			// element size settings
			double h0_;

			// fixed node positions [x,y]
			arma::Mat<double> pfix_ext_ = arma::Mat<double>(0,2);

			// distance function
			// negative inside positive outside
			ShDistFunPr fd_;

			// scaling function
			ShDistFunPr fh_;

		// mesher settings
		// do not touch
		private:
			// rolerances
			double dptol_ = 0.001; 
			double ttol_ = 0.1; 
			double Fscale_ = 1.2; 
			double deltat_ = 0.2; 

			// quad recombination
			double recombination_treshold_ = 0.1;
			bool alternate_recombination_ = false;

			// iterations
			arma::uword densityctrlfreq_ = 30;
			arma::uword maxiter_ = 1000;

		// internal storage
		private:
			// derived tolerances
			double deps_; // = std::sqrt(arma::datum::eps)*h0
			double geps_; // = 0.001*h0; 

			// fixed points
			arma::Mat<double> pfix_;

			// bounding box [x,y]
			arma::Mat<double>::fixed<2,2> bbox_;

			// stored mesh
			arma::Mat<arma::uword> qt_; // quads or triangles [p1,p2,p3,(p4)]
			arma::Mat<double> p_; // 2D points [x,y]

			// edges and surface edges
			arma::Mat<arma::uword> e_;
			arma::Mat<arma::uword> es_; 
			arma::Mat<arma::uword> ei_; 

			// surface nodes
			arma::Col<arma::uword> ps_;


		// methods
		public:
			// constructcor
			DistMesh2D();

			// destructor
			virtual ~DistMesh2D(){};

			// factory
			static ShDistMesh2DPr create();


			// getting and setting
			// set element size
			void set_h0(const double h0);

			// recombine triangles to quad
			void set_quad(const bool quad);

			// set distance function
			void set_distfun(ShDistFunPr fd);

			// set scaling function
			void set_scalefun(ShDistFunPr fh);

			// set maximum number of iterations
			void set_maxiter(const arma::uword maxiter);

			// set fixed points
			void set_fixed(const arma::Mat<double> &pfix_ext);

			// get mesh, nodes and elements
			arma::Mat<arma::uword> get_elements() const;
			arma::Mat<double> get_nodes() const;
			arma::Mat<arma::uword> get_surface_edges() const;
			arma::Mat<arma::uword> get_internal_edges() const;
			arma::Col<arma::uword> get_surface_node_idx() const;


			// main mesher
			// create edge list
			void create_edges();

			// algorithm for converting a tri-mesh to a quad only mesh
			void tri2quad();

			// smoothing (used for quad mesh only)
			void smoothen();

			// method for removing duplicates from a mesh
			void remove_duplicate();
			void fix_clockwise();

			// main distmesh algorithm
			void setup();

			// export to gmsh
			void export_gmsh(cmn::ShGmshFilePr gmsh) const;

			// functionf for calculating area
			double calc_area() const;


			// delaunay functions
			// function for checking points in circle
			static bool circumcircle(arma::Row<double>::fixed<2> &pc, double &r, const arma::Row<double>::fixed<2> &p, const arma::Mat<double>::fixed<3,2> &ptri);

			// delaunay triangulation function
			static arma::Mat<arma::uword> triangulation(const arma::Mat<double> &p);

			// section find math function
			static arma::Mat<arma::uword> find_sections(const arma::Row<arma::uword> &M);


			// distmesh helper functions
			// meshgrid for generating initial coordinates
			static void meshgrid(arma::Mat<double> &x, arma::Mat<double> &y, const arma::Row<double> &xgv, const arma::Col<double> &ygv);

			// triangular to quad helper functions		
			// function for creating graph of all neighbouring elements
			static void element_graph(arma::Col<arma::uword> &row, arma::Col<arma::uword> &col, const arma::Mat<arma::uword> &t);

			// function for calculating the quality of elements
			static arma::Col<double> element2quality(const arma::Mat<arma::uword> &tq, const arma::Mat<double> &p);
			static arma::Col<double> element2quality_alt(const arma::Mat<arma::uword> &tq, const arma::Mat<double> &p);

			// algorithm for merging two triangles into one quad
			static arma::Row<arma::uword>::fixed<4> merge2triangles(const arma::Row<arma::uword>::fixed<3> &t1, const arma::Row<arma::uword>::fixed<3> &t2);

			// algorithm for performing catmull clark subdivision
			static void refine(arma::Mat<arma::uword> &qr, arma::Mat<double> &pr, const arma::Mat<arma::uword> &qt, const arma::Mat<double> &p);


			// serialization and deserializetion of input settings
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
