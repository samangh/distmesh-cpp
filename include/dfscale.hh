/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_DF_SCALE_HH
#define DM_DF_SCALE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "distfun.hh"

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class DFScale> ShDFScalePr;

	// circle distance function for distmesh
	class DFScale: public DistFun{
		// properties
		private:
			// scaling
			double offset_;
			double scale_;
			double max_;

			// boundaries
			ShDistFunPr df_;

		// methods
		public:
			// constructcor
			DFScale(){};
			DFScale(ShDistFunPr df, const double offset, const double scale, const double max = 1e99);

			// factory
			static ShDFScalePr create(ShDistFunPr df, const double offset, const double scale, const double max = arma::datum::inf);

			// get bounding box
			virtual arma::Mat<double>::fixed<2,2> get_bounding() const override;

			// fixed points
			virtual arma::Mat<double> get_fixed() const override;

			// distance function
			virtual arma::Col<double> calc_distance(const arma::Mat<double> &p) const override;

			// serialization
			static std::string get_type(); 
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif
