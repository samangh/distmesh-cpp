/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_SERIALIZER_HH
#define DM_SERIALIZER_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include <boost/filesystem.hpp>

// code specific to Rat
namespace rat{namespace dm{

	// shared pointer definition
	typedef std::shared_ptr<class Serializer> ShSerializerPr;

	// use a class for storage
	class Serializer: virtual public rat::cmn::Serializer{
		public:
			// constructor
			Serializer();
			Serializer(const boost::filesystem::path &fname);

			// factory
			static ShSerializerPr create();
			static ShSerializerPr create(const boost::filesystem::path &fname);

			// registry
			virtual void register_constructors() override;

			// instantiate conductor from file
			template<typename T> static std::shared_ptr<T> create(const boost::filesystem::path &fname);
	};

	// instantiate conductor from file
	template<typename T> std::shared_ptr<T> Serializer::create(const boost::filesystem::path &fname){
		ShSerializerPr slzr = Serializer::create(fname);
		return slzr->construct_tree<T>();
	}

}}

#endif