/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>

#include "distmesh2d.hh"
#include "dfcircle.hh"
#include "dfrectangle.hh"
#include "dfscale.hh"
#include "dfdiff.hh"

// main
int main(){
	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(0.05);

	// the distance function
	mymesh->set_distfun(rat::dm::DFDiff::create(rat::dm::DFRectangle::create(-1,1,-1,1),rat::dm::DFCircle::create(0.5,0,0)));

	// the scale function
	mymesh->set_scalefun(rat::dm::DFScale::create(rat::dm::DFCircle::create(0.5,0,0),0.05,0.3));
	

	// settings
	const double hole_radius = 0.5;
	const double square_sides = 2.0;
	const double min_elements = 0.05;
	const double scale = 0.3;
	const double tol = 1e-2;

	// theoretical area
	const double Ath = square_sides*square_sides - arma::datum::pi*hole_radius*hole_radius;

	// try both quad an tri-meshes
	for(arma::uword quad=0;quad<2;quad++){
		// create mesh
		rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();
		mymesh->set_quad(quad);

		// initial edge length
		mymesh->set_h0(min_elements);

		// the distance function
		mymesh->set_distfun(rat::dm::DFDiff::create(
			rat::dm::DFRectangle::create(-square_sides/2,square_sides/2,-square_sides/2,square_sides/2),
			rat::dm::DFCircle::create(hole_radius,0,0)));

		// the scale function
		mymesh->set_scalefun(rat::dm::DFScale::create(rat::dm::DFCircle::create(hole_radius,0,0),min_elements,scale));

		// setup mesh
		mymesh->setup();

		// calculate mesh area
		const double Amsh = mymesh->calc_area();

		// compare
		if(std::abs(Ath-Amsh)/Ath>tol){
			rat_throw_line("calculated mesh area does not match theory");
		}
	}



	return 0;
}