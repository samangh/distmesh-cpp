/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <algorithm>

// command line parser
#include <tclap/CmdLine.h>

// headers for common
#include "rat/common/defines.hh"
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"

// headers for distmesh
#include "distmesh2d.hh"
#include "serializer.hh"

// main
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Distmesh Library",' ',"1.0");

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> input_filename_argument(
		"if","Input filename",true, "json/geometry/moon.json", "string", cmd);
	
	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> output_filename_argument(
		"of","Output filename",false, "moon.gmsh", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);
	
	// get file name
	const boost::filesystem::path ifname = input_filename_argument.getValue();

	// create serialization object
	rat::dm::ShSerializerPr slzr = rat::dm::Serializer::create(ifname);

	// export
	rat::dm::ShDistMesh2DPr dmsh = slzr->construct_tree<rat::dm::DistMesh2D>(); 

	// run mesher
	dmsh->setup();

	// output to gmsh file
	if(output_filename_argument.isSet()){
		const boost::filesystem::path ofname = output_filename_argument.getValue();
		dmsh->export_gmsh(rat::cmn::GmshFile::create(ofname));
	}

	// done
	return 0;
}