/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>

#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"

#include "dfpolygon.hh"
#include "distmesh2d.hh"
#include "dfscale.hh"
#include "dfcircle.hh"

// main
int main(){
	// create mesh
	rat::dm::ShDistMesh2DPr mymesh = rat::dm::DistMesh2D::create();

	// initial edge length
	mymesh->set_h0(6e-3);
	mymesh->set_quad(true);

	// arma::Mat<double> v(6,2);
	// v.row(0) = arma::Row<double>::fixed<2>{-0.0050,0.0130};
	// v.row(1) = arma::Row<double>::fixed<2>{-0.0050,0.0316};
	// v.row(2) = arma::Row<double>::fixed<2>{0.1733,0.0057};
	// v.row(3) = arma::Row<double>::fixed<2>{0.1733,0};
	// v.row(4) = arma::Row<double>::fixed<2>{0.0683,0};
	// v.row(5) = arma::Row<double>::fixed<2>{0.0683,0.0130};

	// wedge
	arma::Mat<double> v(4,2);
	v.row(0) = arma::Row<double>::fixed<2>{0,0};
	v.row(1) = arma::Row<double>::fixed<2>{0.2,0.0};
	v.row(2) = arma::Row<double>::fixed<2>{0.2,0.005};
	v.row(3) = arma::Row<double>::fixed<2>{0,0.02};
	
	// the distance function
	mymesh->set_distfun(rat::dm::DFPolygon::create(arma::flipud(v)));

	// setup mesh
	mymesh->setup();
	
	// get mesh
	arma::Mat<arma::uword> q = mymesh->get_elements();
	arma::Mat<double> p = mymesh->get_nodes();
	arma::Mat<arma::uword> es = mymesh->get_surface_edges();

	// write to gmsh
	mymesh->export_gmsh(rat::cmn::GmshFile::create("structure.gmsh"));

	// write to output file
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	slzr->flatten_tree(mymesh); slzr->export_json("structure.json");

	return 0;
}