/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_FACTORY_LIST_HH
#define DM_FACTORY_LIST_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include "distmesh2d.hh"
#include "dfairfoil.hh"
#include "dfcircle.hh"
#include "dfdiff.hh"
#include "dfellipse.hh"
#include "dfintersect.hh"
#include "dfones.hh"
#include "dfpolygon.hh"
#include "dfrectangle.hh"
#include "dfrrectangle.hh"
#include "dfscale.hh"
#include "dfunion.hh"

// code specific to Rat
namespace rat{namespace mat{

	// use a class for storage
	class FactoryList{
		public:
			static void register_constructors(rat::cmn::ShSerializerPr slzr){
				// register all required constructors
				register_factory<rat::dm::DistMesh2D>();
				register_factory<rat::dm::DFAirFoil>();
				register_factory<rat::dm::DFCircle>();
				register_factory<rat::dm::DFDiff>();
				register_factory<rat::dm::DFEllipse>();
				register_factory<rat::dm::DFIntersect>();
				register_factory<rat::dm::DFOnes>();
				register_factory<rat::dm::DFPolygon>();
				register_factory<rat::dm::DFRectangle>();
				register_factory<rat::dm::DFRRectangle>();
				register_factory<rat::dm::DFScale>();
				register_factory<rat::dm::DFUnion>();
			}
	};

}}

#endif



