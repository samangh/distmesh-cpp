/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DM_GMSH_FILE_HH
#define DM_GMSH_FILE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

// shared pointer definition for log
typedef std::shared_ptr<class DMGmshFile> ShDMGmshFilePr;

// logging to the terminal
class DMGmshFile{
	// properties
	private:
		// settings
		std::ofstream fid_;

	// methods 
	public:
		// constructor
		DMGmshFile(const std::string &fname);

		// destructor
		~DMGmshFile();

		// factory
		static ShDMGmshFilePr create(const std::string &fname);

		// write nodes
		void write_nodes(const arma::Mat<double> &Rn);

		// write elements
		void write_elements(const arma::Mat<arma::uword> &n);
		void write_elements(const arma::Mat<arma::uword> &n, const arma::Mat<arma::uword> &s);

		// write scalar-data at nodes
		void write_nodedata(const arma::Mat<double> &v,const std::string &datname);

		// write vector-data at elements
		void write_elementdata(const arma::Mat<double> &v,const std::string &datname);
};


#endif